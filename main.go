package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"gitlab.com/mirrorgleam/SteamAPITesting/profile"
)

var (
	steamName string
	steamKey  string
)

func main() {

	steamID := profile.GetSteamID(steamName)

	profileData := profile.GetProfile(steamID, steamKey)

	accountCreated := int64(profileData.Response.Players[0].Timecreated)

	allJSONList := profile.GetFriendsList(steamID, steamKey)

	// Faster way to resolve all friends usernames from ID
	for _, item := range allJSONList.Response.Players {
		fmt.Println("UserName :", item.Personaname)
		fmt.Println("Steam ID :", item.Steamid)
		fmt.Println("")
	}

	fmt.Println(len(allJSONList.Response.Players))

	// Print username
	fmt.Printf("User Name: %s\n", profileData.Response.Players[0].Personaname)
	// Print SteamID
	fmt.Printf("SteamID #: %s\n", profileData.Response.Players[0].Steamid)
	// Print date and time account was created
	fmt.Printf("Created  : %s\n", time.Unix(accountCreated, 0))
}

func init() {
	flag.StringVar(&steamName, "name", "", "Steam account name to look up")
	flag.StringVar(&steamKey, "key", "", "Your Steam API key")
	flag.Parse()

	if flag.NFlag() < 2 {
		fmt.Printf("Usage: %s [options]\n", os.Args[0])
		fmt.Println("Options:")
		flag.PrintDefaults()
		os.Exit(1)
	}
}
