// Build out struct for response and functions for
// retriving profile data from Steam

package profile

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
)

// PlayerSummary structure for a player profile from a call to:
// http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/
type PlayerSummary struct {
	Response struct {
		Players []struct {
			Steamid                  string `json:"steamid"`
			Communityvisibilitystate int    `json:"communityvisibilitystate"`
			Profilestate             int    `json:"profilestate"`
			Personaname              string `json:"personaname"`
			Lastlogoff               int    `json:"lastlogoff"`
			Profileurl               string `json:"profileurl"`
			Avatar                   string `json:"avatar"`
			Avatarmedium             string `json:"avatarmedium"`
			Avatarfull               string `json:"avatarfull"`
			Personastate             int    `json:"personastate"`
			Primaryclanid            string `json:"primaryclanid,omitempty"`
			Timecreated              int    `json:"timecreated,omitempty"`
			Personastateflags        int    `json:"personastateflags,omitempty"`
			Loccountrycode           string `json:"loccountrycode,omitempty"`
			Locstatecode             string `json:"locstatecode,omitempty"`
			Loccityid                int    `json:"loccityid,omitempty"`
		} `json:"players"`
	} `json:"response"`
}

// FriendList is the struct for the JSON returned from
// querying the Steam API for the friends list
type FriendList struct {
	Friendslist struct {
		Friends []struct {
			Steamid      string `json:"steamid"`
			Relationship string `json:"relationship"`
			FriendSince  int    `json:"friend_since"`
		} `json:"friends"`
	} `json:"friendslist"`
}

// GetProfile makes a call to Steam API and returns the
// PlayerSummary struct related to the given SteamID.
func GetProfile(SteamID string, SteamKey string) PlayerSummary {

	// Instantiate Response struct
	var profile PlayerSummary

	// Build query URL using key and id to lookup
	url := fmt.Sprintf("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=%s&steamids=%s", SteamKey, SteamID)

	// Perform http Get request
	resp, err := http.Get(url)
	if err != nil {
		resp.Body.Close()
		log.Panic(err)
	}
	defer resp.Body.Close()

	// Write response to body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Panic(err)
	}

	// Unmarshal body data to profile PlayerSummary Struct
	json.Unmarshal(body, &profile)

	return profile
}

// GetSteamID takes a Steam UserName and returns a Steam
// unique identifier as a string. If a UserName isn't found
// returns a blank string.
func GetSteamID(gamerTag string) string {
	url := fmt.Sprintf("https://steamcommunity.com/id/%s/", gamerTag)

	steamID := ""

	resp, err := http.Get(url)
	if err != nil {
		log.Panic(err)
	}
	defer resp.Body.Close()

	// Find the unique Steam ID on the page hidden in the JS code
	scanner := bufio.NewScanner(resp.Body)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), "g_rgProfileData") {
			re := regexp.MustCompile(`\d{17}`)
			line := scanner.Text()
			steamID = re.FindString(line)
			break
		}
	}
	if steamID == "" {
		fmt.Printf("User %s not found\n", gamerTag)
		os.Exit(1)
	}

	return steamID

}

// GetFriendsList takes a users SteamKey and a SteamID to lookup and
// returns the friends list of the given SteamID
func GetFriendsList(SteamID string, SteamKey string) PlayerSummary {

	var List FriendList

	var friendStringList []string

	// Build query URL using key and id to lookup
	url := fmt.Sprintf("http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key=%s&steamid=%s&relationship=friend", SteamKey, SteamID)

	// Perform http Get request
	resp, err := http.Get(url)
	if err != nil {
		resp.Body.Close()
		log.Panic(err)
	}
	defer resp.Body.Close()

	// Write response to body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Panic(err)
	}

	// Unmarshal body data to profile Response Struct
	json.Unmarshal(body, &List)

	// Build slice of friend SteamIDs
	for _, item := range List.Friendslist.Friends {
		friendStringList = append(friendStringList, item.Steamid)
	}

	// Convert slice of friend SteamIDs to comma seperated list
	// for use in GetProfile url
	friendsString := strings.Join(friendStringList, ",")

	friendsList := GetProfile(friendsString, SteamKey)

	return friendsList

}
