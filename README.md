# **Steam API Data Retrival and Processing with Golang**

![My Steam Profile Pic](https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/94/94065675da8b9f37d16cac6673a4576302d00dff_full.jpg)


My intention with this project is to become more fluent
with Go through practice on Steam's public API data.


### Usage of tool:
1. Have Golang installed and configured
2. Clone the code locally
3. Perform go build on main.go
4. ./SteamAPITesting -key "YOUR_STEAM_API_KEY" -name SteamAccountNameYouWantToLookUp


### Resources

* [Steam API Docs](https://developer.valvesoftware.com/wiki/Steam_Web_API)
* [JSON to Go](https://mholt.github.io/json-to-go/)
* [Subcommand Flags](http://blog.ralch.com/tutorial/golang-subcommands/)


### Future Potential Use Cases
This list will grow as I learn more of the data available to me.

Currently I know I can pull friends names and games lists.

* List Friends Alphabetically: 
    * Print an alphabetical list of the selected user's friends
    * Syntax- `SteamAPITesting friends -key $STEAM_KEY -profile accountName -alpha`
* List Friends By Time Friended:
    * Print a list of the users friends ordered by time friended
    * Syntax- `SteamAPITesting friends -key $STEAM_KEY -profile accountName -friended`
* Friend Roulette: 
    * Pull list of user's friends and select one at random
    * Syntax- `SteamAPITesting friends -key $STEAM_KEY -profile accountName -random`
* List Games Alphabetically:
    * Print an alphabetical list of the selected user's games
    * Syntax- `SteamAPITesting games -key $STEAM_KEY -profile accountName -alpha`
* List Games By Time Played:
    * Print a list of the users games ordered by time played
    * Syntax- `SteamAPITesting games -key $STEAM_KEY -profile accountName -playtime`
* Game Roulette:
    * Pull list of user's games and select one at random
    * Syntax- `SteamAPITesting games -key $STEAM_KEY -profile accountName -random`
    